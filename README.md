# forecast
Short one line bash script to get the weather forecast for a given location.
It is designed to be an add on script for the Chameleon conky script.

To change the location change the location INZ004 to your National Weather Service Zone.

The script gives the current forcast and the forecast for the next time period.  To increase the number of time periods change 
select(.number<=2) to a higher integer.

Script requires curl and jq to run.
